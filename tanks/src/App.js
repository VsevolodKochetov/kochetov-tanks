import React, { useState, useEffect, useRef } from 'react';
import './App.css';

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {x: 0, y: 0}
  }

  pressHandler = (e) => {
    switch (e.key) {
      case 'w': {
        this.state.y > 0 && this.setState({y: this.state.y - 1})
        break
      }
      case 's': {
        this.state.y < 475 && this.setState({ y: this.state.y + 1 })
        break
      }
      case 'a': {
        this.state.x > 0 && this.setState({ x: this.state.x - 1 })
        break
      }
      case 'd': {
        this.state.x < 475 && this.setState({ x: this.state.x + 1 })
      }
      default:
        return
    }
  }

  componentDidMount(){
    document.addEventListener('keydown', e => this.pressHandler(e))
  }

  render() { 
    return (
      <div className='App'>
        <Tank left={this.state.x} top={this.state.y} />
      </div>
    )
  }
}

const Tank = props => {
  return(
    <div className="Tank" style={{left: `${props.left}px`, top:`${props.top}px`}}>
    </div>
  )
}

export default App;
